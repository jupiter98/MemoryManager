#include "Bint.h"
#include <algorithm>
#include <limits>
#include <cstdio>
#include <inttypes.h>
#include <vector>
#include <cassert>

//useful values for decimal bases. 
constexpr uint64_t max_power10_in_max_power2[] = { 0, 100, 10000, 10000000, 1000000000, 1000000000000, 100000000000000, 10000000000000000, 10000000000000000000 };

//useful getter to get decimal base for an element of base 2^32
constexpr uint64_t max_power10()
{
	return max_power10_in_max_power2[sizeof(uint32_t)];
}

//gets the digit count for a number in base decimal.
template <uint64_t T>
struct count_digits
{
	static constexpr uint64_t value = 1 + count_digits<T / 10>::value;
};

//trait for 0
template <>
struct count_digits<0>
{
	static constexpr uint64_t value = 0;
};

constexpr uint64_t max_digits()
{
	return count_digits<max_power10()>::value;
}
//empty constructor
Bint::Bint() : m_Value(1), isNegative(false)
{
}

Bint::Bint(int32_t val) : m_Value(1)
{
	if (val < 0)
	{
		isNegative = true;
		val = -val;
	}
	else
	{
		isNegative = false;
	}

	m_Value[0] = val;//initialize value and sign correctly
}

Bint::Bint(const std::string& strVal) : Bint()
{
	if (strVal.empty())
	{
		std::cerr << "ERROR BINT(STRING) -- Empty string" << std::endl;
		return;
	}

	std::string numericVal;
	if (strVal.length() > 0 && (strVal[0] == '-' || strVal[0] == '+'))
	{
		isNegative = strVal[0] == '-' ? true : false;

		numericVal = strVal.substr(1);//get numbers without sign
		if (numericVal.length() <= 0)
		{
			std::cerr << "ERROR BINT(STRING) -- there is no numeric value" << std::endl;
			return;
		}
	}
	else
	{
		std::cerr << "ERROR BINT(STRING) -- You must precede the number with a sign!" << std::endl;
		return;
	}

	for (size_t i = 0; i < numericVal.length(); ++i)
	{
		uint32_t digit = numericVal[i] - '0'; //truncation of digit

		*this *= 10; //advance decimal count for each digit 
		Sum(digit);
	}

}
//copy
Bint::Bint(const Bint& other) : m_Value(other.m_Value), isNegative(other.isNegative)
{

}

int Bint::CheckEquality(const Bint& other) const
{
	//the bigger the size, the higher the value
	if (m_Value.size() != other.m_Value.size())
	{
		return m_Value.size() > other.m_Value.size() ? 1 : -1;
	}

	assert(m_Value.size() == other.m_Value.size());
	//compare each digit one by one
	for (size_t i = 0; i < m_Value.size(); i++)
	{
		const size_t index = m_Value.size() - i - 1;

		if (m_Value[index] != other.m_Value[index])
		{
			return m_Value[index] > other.m_Value[index] ? 1 : -1;
		}
	}

	return 0; //they are equal
}

void Bint::Sum(const Bint& other)
{
	//check if the <other> contains more elements
	const size_t resultLenght = std::max(m_Value.size(), other.m_Value.size());
	m_Value.resize(resultLenght);

	uint32_t depositary(0);
	for (size_t index = 0; index < resultLenght; ++index)
	{
		const uint64_t firstOperand = m_Value[index];
		const uint64_t secondOperand = index < other.m_Value.size() ? other.m_Value[index] : 0;

		OpResult Result;
		Result.result = firstOperand + secondOperand + depositary;//add rest from previous calculus.

		m_Value[index] = Result.GetRightmostVal();//lower half
		depositary = Result.GetLeftmostVal();//upper half 
	}
	if (depositary > 0)
	{
		m_Value.push_back(depositary); //add depositary as new element in deque 
	}
}

void Bint::Sub(const Bint& other)
{
	//as of stated by the paper 
	int64_t depositary(0);

	const size_t maxLenght = std::max(m_Value.size(), other.m_Value.size());
	m_Value.resize(maxLenght);

	const bool aIsGreater = CheckEquality(other) >= 0; //take the greater number between the two
	const int64_t aSign = aIsGreater ? +1 : -1; //the greater will be positive in the operation
	const int64_t bSign = -aSign; //obligatory sign check, the smaller number will be the opposite sign
	for (size_t i = 0; i < maxLenght; i++)
	{
		int64_t aNumber = m_Value[i];
		int64_t bNumber = i < other.m_Value.size() ? other.m_Value[i] : 0;
		//A + (-B)
		int64_t result = { (aSign * aNumber) + (bSign * bNumber) + depositary };

		if (result < 0)//underflow
		{
			m_Value[i] = std::numeric_limits<uint32_t>::max() + (result + 1);
			depositary = -1; //remove one from the closest greater decimals.
		}
		else
		{
			m_Value[i] = result;
			depositary = 0;
		}
	}

	while (m_Value.back() == 0 && m_Value.size() > 1)
	{
		m_Value.pop_back();
	}
	//final sign check for the value against first operand.
	//for example if the number was positive and we subtracted a larger number than the result will be negative.
	isNegative = (isNegative == aIsGreater); 
}

Bint Bint::Mul(const Bint& other)
{
	if (other == 1) return *this; //obvious, do nothing 
	if (other == 0) //same, return 0 
	{
		isNegative = false;
		std::deque<uint32_t> newValue(1);
		m_Value = std::move(newValue);
	}

	isNegative = (isNegative && !other.isNegative) || (!isNegative && other.isNegative); //sign check

	uint32_t depositary(0);

	//check if the other deque contains more elements and resize accordingly
	const size_t resultLenght = m_Value.size() + other.m_Value.size() - 1;
	Bint result;
	result.m_Value.resize(resultLenght);

	//get each value of the firstOperand and multiply it by second + depositary.
	for (size_t i = 0; i < m_Value.size(); ++i)
	{
		const uint64_t firstOperand = m_Value[i];

		for (size_t j = 0; j < other.m_Value.size(); ++j)
		{
			const uint64_t secondOperator = other.m_Value[j];

			OpResult opResult;
			opResult.result = (firstOperand * secondOperator) + depositary; //real multiplication

			result.m_Value[i + j] += opResult.GetRightmostVal();
			depositary = opResult.GetLeftmostVal();
		}
	}

	if (depositary > 0)
	{
		result.m_Value.push_back(depositary); //if depositary is positive, add it to the result
	}

	m_Value = std::move(result.m_Value);

	return *this;
}

Bint Bint::Div(const Bint& other)
{
#pragma region No Calculation Needed
	if (other == 0)
	{
		std::cerr << "ERROR BINT::DIV --You cannot divide a number by 0!" << std::endl;
		return Bint();
	}
	if (*this == 0) //division from 0 is always 0
	{
		isNegative = false;
		std::deque<uint32_t> newValue(1);
		m_Value = std::move(newValue);

		return Bint();
	}
	if (other == +1 || other == -1) //this should result in the first number 
	{
		isNegative = isNegative != other.isNegative;
		return Bint();
	}
	if (*this == other) //this is always 1
	{
		isNegative = false;
		std::deque<uint32_t> newValue(1);
		m_Value = std::move(newValue);
		m_Value[0] = 1;

		return Bint();
	}
#pragma endregion

	bool resultSign = isNegative != other.isNegative;
	Bint divisor = other;
	Bint resValue;

	isNegative = false;
	divisor.isNegative = false;

	//Naive implementation, keep subtracting until we reach 0 or less.
	//Slow but sure path.
	while (CheckEquality(other) >= 0)
	{
		Sub(divisor);
		++resValue;
	}
	Bint rest{ *this };
	resValue.isNegative = resultSign;
	m_Value = resValue.m_Value;
	return rest;

}

uint32_t Bint::OpResult::GetRightmostVal() const
{
	return static_cast<uint32_t>(result);
}

uint32_t Bint::OpResult::GetLeftmostVal() const
{
	const uint32_t leftVal = result >> sizeof(uint32_t) * CHAR_BIT;
	return leftVal;
}

Bint::~Bint()
{
}

Bint& Bint::operator=(const Bint& other) {

	if (*this != other)
	{
		m_Value = other.m_Value;
		isNegative = other.isNegative;
	}

	return *this;
}

Bint& Bint::operator+=(const Bint& other)
{
	if (isNegative == other.isNegative) //both have the same sign
	{
		Sum(other);
	}
	else
	{
		Sub(other);
	}

	return *this;
}

Bint& Bint::operator-=(const Bint& other)
{
	if (isNegative == other.isNegative)//both have the same sign
	{
		Sub(other);
	}
	else
	{
		Sum(other);
	}

	return *this;
}

Bint& Bint::operator*=(const Bint& other)
{
	Mul(other);
	return *this;
}

Bint& Bint::operator/=(const Bint& other)
{
	Div(other);
	return *this;
}

Bint& Bint::operator%=(const Bint& other)
{
	m_Value = Div(other).m_Value;
	isNegative = false;
	return *this;
}

Bint operator+(const Bint& other)
{
	return other;
}

Bint operator-(const Bint& other)
{
	Bint result{ other };
	result.isNegative = !other.isNegative;

	return result;
}

Bint operator+(const Bint& firstNumber, const Bint& secondNumber)
{
	Bint result{ firstNumber };

	result += secondNumber;

	return result;
}

Bint operator-(const Bint& firstNumber, const Bint& secondNumber)
{
	Bint result{ firstNumber };

	result -= secondNumber;

	return result;
}

Bint operator*(const Bint& firstNumber, const Bint& secondNumber)
{
	Bint result{ firstNumber };

	result *= secondNumber;

	return result;
}

Bint operator/(const Bint& firstNumber, const Bint& secondNumber)
{
	Bint result{ firstNumber };

	result /= secondNumber;

	return result;
}

Bint operator%(const Bint& firstNumber, const Bint& secondNumber)
{
	return Bint{ firstNumber } %= secondNumber;
}


bool operator<(const Bint& firstNumber, const Bint& secondNumber)
{
	if (firstNumber.isNegative != secondNumber.isNegative)
	{
		return firstNumber.isNegative;
	}

	if (firstNumber.isNegative)
	{
		return firstNumber.CheckEquality(secondNumber) > 0;
	}
	else
	{
		return firstNumber.CheckEquality(secondNumber) < 0;
	}
}

bool operator>(const Bint& firstNumber, const Bint& secondNumber)
{
	return secondNumber < firstNumber;
}

bool operator<=(const Bint& firstNumber, const Bint& secondNumber)
{
	return !(firstNumber > secondNumber);
}

bool operator>=(const Bint& firstNumber, const Bint& secondNumber)
{
	return !(firstNumber < secondNumber);
}

bool operator==(const Bint& firstNumber, const Bint& secondNumber)
{
	//0 = equal
	return firstNumber.CheckEquality(secondNumber) == 0;
}

bool operator!=(const Bint& firstNumber, const Bint& secondNumber)
{
	return !(firstNumber == secondNumber);
}

Bint& Bint::operator++()
{
	*this += 1;
	return *this;
}

Bint Bint::operator++(int)
{
	Bint result(*this);
	++(*this);
	return result;
}

Bint& Bint::operator--()
{
	*this -= 1;
	return *this;
}

Bint Bint::operator--(int)
{
	Bint result(*this);
	--(*this);
	return result;
}

Bint Bint::pow(const Bint& exponent)
{
	Bint res(*this);
	for (int i = 1; i < exponent; i++)
		res *= (*this);
	return res;
}

Bint& Bint::operator&=(const Bint& other) {

	for (size_t i = 0; i < m_Value.size(); i++)
	{
		const uint32_t  otherValue = i < other.m_Value.size() ? other.m_Value[i] : (uint32_t)0;

		m_Value[i] &= otherValue;
	}

	return *this;
}
Bint& Bint::operator|=(const Bint& other) {

	for (size_t i = 0; i < m_Value.size(); i++)
	{
		const uint32_t otherValue = { i < other.m_Value.size() ? other.m_Value[i] : (uint32_t)0 };

		m_Value[i] |= otherValue;
	}

	return *this;
}
Bint& Bint::operator^=(const Bint& other)
{
	for (size_t i = 0; i < m_Value.size(); i++)
	{
		const uint32_t otherValue = { i < other.m_Value.size() ? other.m_Value[i] : (uint32_t)0 };

		m_Value[i] ^= otherValue;
	}

	return *this;
}
Bint& Bint::operator<<=(const Bint& other)
{
	//manage edge cases: this is negative, other is negative, m_value is 0
	if (isNegative || other <= 0)
		return *this;

	Bint otherCpy{ other };

	while (otherCpy > 0)
	{
		uint32_t  shiftNum{ 0 };

		const uint32_t  maxShift = sizeof(uint32_t) * CHAR_BIT;

		if (otherCpy >= maxShift)
		{
			shiftNum = maxShift;
		}
		else
		{
			shiftNum = (uint32_t)otherCpy;
		}

		otherCpy -= maxShift;

		ShiftLeft(shiftNum);
	}

	return *this;
}
Bint& Bint::operator>>=(const Bint& other)
{
	Bint otherCpy{ other };

	while (otherCpy > 0)
	{
		uint32_t  shiftNum{ 0 };

		const uint32_t  maxShift = sizeof(uint32_t) * CHAR_BIT;

		if (otherCpy >= maxShift)
		{
			shiftNum = maxShift;
		}
		else
		{
			shiftNum = (uint32_t)otherCpy;
		}

		otherCpy -= maxShift;

		ShiftRight(shiftNum);
	}

	return *this;
}
void Bint::ShiftLeft(uint32_t shiftNum)
{
	uint32_t rest{ 0 };

	for (size_t i = 0; i < m_Value.size(); i++)
	{
		OpResult opResult = { m_Value[i] };
		opResult.result <<= shiftNum;
		opResult.result |= rest;

		m_Value[i] = opResult.GetRightmostVal();
		rest = opResult.GetLeftmostVal();
	}

	if (rest > 0)
	{
		m_Value.push_back(rest); //The shift left operation is actually inserting an element to the head of integer queue
	}
}
void Bint::ShiftRight(uint32_t shiftNum)
{
	uint32_t rest{ 0 };

	for (size_t i = 0; i < m_Value.size(); i++)
	{
		const size_t index = m_Value.size() - i - 1;
		const uint32_t maxShift = sizeof(uint32_t) * CHAR_BIT;

		OpResult opResult = { m_Value[index] };
		opResult.result <<= maxShift;

		opResult.result >>= shiftNum;

		m_Value[index] |= rest;

		m_Value[index] = opResult.GetLeftmostVal();
		rest = opResult.GetRightmostVal();
	}

	if (m_Value.back() == 0 && m_Value.size() > 1)
	{
		m_Value.pop_back();
	}
}

std::string Bint::IntToString(uint64_t num) {
	constexpr uint64_t maxzeroes = max_digits();
	std::string format = "%0";
	format.append(std::to_string(maxzeroes - 1));
	format.append(PRIu64);

	char buffer[maxzeroes];
	sprintf_s(buffer, maxzeroes, format.c_str(), num);

	return buffer;
}
std::ostream& operator<<(std::ostream& stream, const Bint& bInt)
{
	std::vector<std::string> result;

	Bint a{ bInt };
	a.isNegative = false;

	constexpr uint64_t divisor = max_power10();
	while (a > divisor)
	{
		//a /= divisor;//? does not work with big numbers, maybe too slow?
		const Bint rest = a.Div(divisor);
		const std::string resultDigit = Bint::IntToString(rest.m_Value[0]);
		result.push_back(resultDigit);
	}

	const std::string resultDigit = std::to_string(a.m_Value[0]); //Last rest
	result.push_back(resultDigit);

	if (bInt.isNegative)
	{
		result.push_back("-");
	}
	else
	{
		result.push_back("+");
	}

	std::reverse(result.begin(), result.end());//flip

	for (size_t i = 0; i < result.size(); i++)
	{
		stream << result[i];
	}

	return stream;
}

Bint::operator uint32_t() const
{
	return m_Value[0];
}

