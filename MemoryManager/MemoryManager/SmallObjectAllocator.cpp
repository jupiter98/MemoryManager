
#include "SmallObjectAllocator.h"

//default constructor.
SmallObjAllocator::SmallObjAllocator(
	std::size_t chunkSize,
	std::size_t maxObjectSize)
	: m_pLastAlloc(0), m_pLastDealloc(0)
	, m_chunkSize(chunkSize), m_maxObjectSize(maxObjectSize) {}

//old one was a binary function, but is now deprecated.
struct CompareFixedAllocatorSize
{
	bool operator()(const FixedAllocator& x, std::size_t numBytes) const
	{
		return x.GetBlockSize() < numBytes;
	}
};

void* SmallObjAllocator::Allocate(std::size_t numBytes)
{

	//check if we already allocated an allocator, and if this one manage Chunk of the desired size
	if (!m_pLastAlloc || m_pLastAlloc->GetBlockSize() != numBytes)
	{
		//gets the first fixed allocator that manages big enough data to contain what I'm trying to allocate.
		Pool::iterator i = std::lower_bound(m_pool.begin(), m_pool.end(), numBytes, CompareFixedAllocatorSize());

		//if we do not find a suitable allocator, create a new one that does the job
		if (i == m_pool.end() || i->GetBlockSize() != numBytes)
		{
			i = m_pool.insert(i, FixedAllocator(numBytes));
			m_pLastDealloc = &*m_pool.begin();
		}
		m_pLastAlloc = &*i;
	}

	if (m_pLastAlloc && m_pLastAlloc->GetBlockSize() == numBytes)
	{
		return m_pLastAlloc->Allocate();
	}
	return m_pLastAlloc;
}


void SmallObjAllocator::Deallocate(void* p, std::size_t size)
{
	//check if we deallocated something and if the block size corresponds, if yes than use it to deallocate.
	if (m_pLastDealloc && m_pLastDealloc->GetBlockSize() == size)
	{
		m_pLastDealloc->Deallocate(p);
		return;
	}
	//find the allocator used for the deallocation.
	Pool::iterator i = std::lower_bound(m_pool.begin(), m_pool.end(), size, CompareFixedAllocatorSize());
	//check if the allocator is of the right size
	assert(i != m_pool.end());
	assert(i->GetBlockSize() == size);
	m_pLastDealloc = &*i;
	m_pLastDealloc->Deallocate(p);
}

