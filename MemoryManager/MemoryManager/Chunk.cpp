#include "Chunk.h"

//initialize block size for each chunk and the number of blocks
void Chunk::Init(std::size_t blockSize, unsigned char blocks)
{
	//block size has to be grater than 0 
	assert(blockSize > 0);
	//at least a block must be present
	assert(blocks > 0);
	//check for memory overflow
	assert((blockSize * blocks) / blockSize == blocks);
	//memory allocation.
	m_pData = static_cast<unsigned char*>(malloc(blockSize * blocks));
	m_firstAvailableBlock = 0;
	m_blocksAvailable = blocks;
	unsigned char* p = m_pData;
	for (unsigned char i = 0; i != blocks; p += blockSize) //CLEVER!
	{
		//store in each element of the chunk the index of the next one, so that we can have a single linked list w/o overhead.
		*p = ++i;
	}
}

void Chunk::Release()
{
	if (m_pData != nullptr)
	{
		free(m_pData);
	}
}

void* Chunk::Allocate(std::size_t blockSize)
{
	if (!m_blocksAvailable) return 0;
	//should not overflow
	assert((m_firstAvailableBlock * blockSize) / blockSize == m_firstAvailableBlock);

	unsigned char* pResult = m_pData + (m_firstAvailableBlock * blockSize); //we move pdata to the next position and set it to pResult.
	m_firstAvailableBlock = *pResult; //this new block is now the new first available.

	--m_blocksAvailable;

	return pResult;
}

void Chunk::Deallocate(void* p_block, std::size_t blockSize)
{
	assert(p_block >= m_pData);// make sure that the given p is inside the current chunk

	unsigned char* toRelease = static_cast<unsigned char*>(p_block); //interpret as unsigned char

	// Check if the chunk is aligned and all blocks of the chunks are of the same block size
	assert((toRelease - m_pData) % blockSize == 0);

	*toRelease = m_firstAvailableBlock;
	m_firstAvailableBlock = static_cast<unsigned char>((toRelease - m_pData) / blockSize);
	// Truncation check
	assert(m_firstAvailableBlock == (toRelease - m_pData) / blockSize);

	++m_blocksAvailable;
}
