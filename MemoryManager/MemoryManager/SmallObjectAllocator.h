
#include "FixedAllocator.h"
#include "CustomAllocator.h"
//Groups multiple fixed allocators, each specialized to allocate an object of one size. 
class SmallObjAllocator
{
public:
	SmallObjAllocator(std::size_t chunkSize, std::size_t maxObjectSize);

	void* Allocate(std::size_t numBytes);
	//The size used in the deallocation helps us reduce the time needed to find the correct object to deallocate.
	//If it was not present, we would need to search each allocator and each chunk for that object (quadratic complexity)
	void Deallocate(void* p, std::size_t size);

private:
	SmallObjAllocator(const SmallObjAllocator&);
	SmallObjAllocator& operator=(const SmallObjAllocator&) = delete;
	//same strategy as fixed allocator.
	typedef std::vector<FixedAllocator, Mallocator<FixedAllocator> > Pool;
	Pool m_pool;
	FixedAllocator* m_pLastAlloc;
	FixedAllocator* m_pLastDealloc;
	std::size_t m_chunkSize;
	std::size_t m_maxObjectSize;
};
