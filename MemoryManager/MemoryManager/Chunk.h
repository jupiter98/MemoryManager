#pragma once
#include <cstddef>
#include <cassert>
#include <stdlib.h>

//[As of chapter 4.4 from Modern C++ Design]

//Structure that manages a memory chunk consisting of a fixed number of blocks.
//It is a simple structure, it gives us the ability to allocate or deallocate blocks of memory. 
// 
//main github source https://github.com/snaewe/loki-lib


// Nothing is private � Chunk is a Plain Old Data (POD) structure
struct Chunk
{
	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate(std::size_t blockSize);
	void Deallocate(void* p, std::size_t blockSize);
	void Release();//added custom delete function to better handle memory deletion
	unsigned char* m_pData;
	unsigned char m_firstAvailableBlock;
	unsigned char m_blocksAvailable;
};