#pragma once
#include <cstdlib>
#include "CustomMemoryManager.h"


#ifdef USE_MM
void* operator new (size_t objSize)
{
	return MM_NEW(objSize);
}

void* operator new[](size_t aLenght, size_t objSize)
{
	return MM_NEW_A(aLenght, objSize);
}

void operator delete (void* ptr, size_t size)
{
	return MM_DELETE(ptr, size);
}

void operator delete[](void* ptr, size_t aLenght, size_t objSize)
{
	return MM_DELETE_A(ptr, aLenght, objSize);
}

#endif // USE_MM


