#include <iostream>
#include "GlobalMemoryManager.h"
//#define USE_MM
#include <ctime>
#include <chrono>
#include <string>
#include "Bint.h"

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;

struct SmallObject {
	long long firstElement;
	short secondElement;
	char thirdElement;
	bool fourthElement;
	bool fifthElement;
};
void MMPerformanceTest()
{
	std::vector<void*> Pointers;
	auto start_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	for (int i = 0; i < 1000000; ++i)
	{
		void* ptr = MM_NEW(sizeof(SmallObject));
		//SmallObjTest* p = new (ptr) SmallObjTest();
		Pointers.push_back(ptr);
	}

	for (int i = 0; i < 1000000; ++i)
	{
		MM_DELETE(Pointers[i], sizeof(SmallObject));
	}
	auto end_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	long long delta = end_millisec - start_millisec;

	std::cout << "SmallObjAllocator takes :" << std::to_string((float)delta / 1000) << " to complete" << std::endl; //convert to seconds

	std::vector<SmallObject*> Pointers2;

	start_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	for (int i = 0; i < 1000000; ++i)
	{
		SmallObject* ptr = new SmallObject();
		Pointers2.push_back(ptr);
	}
	for (int i = 0; i < 1000000; ++i)
	{
		delete Pointers2[i];
	}
	end_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	delta = end_millisec - start_millisec;
	std::cout << "Default Allocator takes :" << std::to_string((float)delta / 1000) << " to complete" << std::endl; //convert to seconds
}
bool BIntTest()
{
	Bint a("+12345678910111213");
	Bint b("+11111111111111111");
	Bint abResult("+23456790021222324");
	Bint c("-42424242424242442");
	Bint d("-98765432112345678");
	Bint cdResult("+56341189688103236");
	Bint e("+15");
	Bint f("+18");
	Bint efResult("+270");
	Bint j("+10000000000000000000001");
	Bint k("+1000000000000000000000");
	Bint jkResult("+10");

	a += b;
	c -= d;
	e *= f;
	j /= k;

	Bint l("+100000000000000000");
	Bint m("+200000000000000000");
	Bint lmResult("+300000000000000000");
	Bint n("+1000");
	Bint o("+101");
	Bint noResult("+9");

	return  (a == abResult) && (c == cdResult) && (e == efResult) && (j == jkResult)
			&& 
			(l + m == lmResult) && (n / o == noResult);

}

int main()
{

	MMPerformanceTest();

	std::cout << " ---------------- " << std::endl;
#pragma region Bint Test

	std::string result = "";
	if (BIntTest())
	{
		result.append("*SUCCESS*");
	}
	else
	{
		result.append("*FAILURE*");
	}
	std::cout << "Bint Test returned " << result << std::endl;

	return 0;
#pragma endregion
}
