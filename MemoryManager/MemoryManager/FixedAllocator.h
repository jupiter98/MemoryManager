#pragma once
#include "Chunk.h"
#include <vector>
#include "CustomAllocator.h"
#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 4096
#endif

#ifndef MAX_SMALL_OBJECT_SIZE
#define MAX_SMALL_OBJECT_SIZE 64
#endif
//Uses Chunks as a brick. It will have an array of different chunks and handles allocation of a very specific size.
class FixedAllocator
{
private:

	// Internal functions        
	void DoDeallocate(void* p);
	Chunk* CheckVicinity(void* p);

	// Data 
	std::size_t m_blockSize;
	unsigned char m_numBlocks;//blocks in a chunk
	typedef std::vector<Chunk, Mallocator<Chunk>> Chunks;
	Chunks m_chunks;

	Chunk* m_allocChunk = nullptr;//the last chunk allocated (increases speed lookup)
	Chunk* m_deallocChunk = nullptr;//the last chunk deallocated (when deallocating it increases search speed for the correct chunk to use)

	//For ensuring proper copy semantics
	mutable const FixedAllocator* m_prev;
	mutable const FixedAllocator* m_next;

public:
	// Create a FixedAllocator able to manage blocks of 'blockSize' size
	explicit FixedAllocator(std::size_t blockSize = 0);
	FixedAllocator(const FixedAllocator&); //cpy ctr
	~FixedAllocator();

	// Allocate a memory block
	void* Allocate();
	// Deallocate a memory block previously allocated with Allocate()
	// (if that's not the case, the behavior is undefined)
	void Deallocate(void* p);
	// Returns the block size with which the FixedAllocator was initialized with
	std::size_t GetBlockSize() const
	{
		return m_blockSize;
	}
};