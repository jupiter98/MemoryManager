#include "CustomMemoryManager.h"

SmallObjAllocator SOA(DEFAULT_CHUNK_SIZE, MAX_SMALL_OBJECT_SIZE);

void* CustomMemoryManager::CustomNew(size_t size)
{
	if (size > MAX_SMALL_OBJECT_SIZE)
	{
		return ::operator new(size);
	}
	else
	{
		return SOA.Allocate(size);
	}
}

void* CustomMemoryManager::CustomNewA(size_t aLenght, size_t objSize)
{

	if ((aLenght * objSize) > MAX_SMALL_OBJECT_SIZE)
	{
		return ::operator new[]((aLenght* objSize));
	}
	else
	{
		return SOA.Allocate((aLenght * objSize));
	}
}

void CustomMemoryManager::CustomDelete(void* ptr, size_t size)
{
	if (size > MAX_SMALL_OBJECT_SIZE)
	{
		return::operator delete(ptr);
	}
	else
	{
		return SOA.Deallocate(ptr, size);
	}
}

void CustomMemoryManager::CustomDeleteA(void* ptr, size_t aLenght, size_t objSize)
{
	if ((aLenght * objSize) > MAX_SMALL_OBJECT_SIZE)
	{
		return::operator delete[](ptr);
	}
	else
	{
		return SOA.Deallocate(ptr, (aLenght * objSize));
	}
}

void CustomMemoryManager::CustomFree(void* ptr, size_t objectSize)
{
	if (objectSize > MAX_SMALL_OBJECT_SIZE)
	{
		return::free(ptr);
	}
	else
	{
		return SOA.Deallocate(ptr, objectSize);
	}
}

void* CustomMemoryManager::CustomMalloc(size_t objectSize)
{
	if (objectSize > MAX_SMALL_OBJECT_SIZE)
	{
		return std::malloc(objectSize);
	}
	else
	{
		return SOA.Allocate(objectSize);
	}
}
