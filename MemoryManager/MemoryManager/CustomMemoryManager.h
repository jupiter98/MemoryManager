#pragma once
#include <cstddef>
#include <new>
#include "SmallObjectAllocator.h"

#define MM_NEW(SIZE) CustomMemoryManager::getInstance().CustomNew(SIZE);
#define MM_NEW_A(LENGHT, SIZE) CustomMemoryManager::getInstance().CustomNewA(LENGHT , SIZE);

#define MM_DELETE(PTR, SIZE) CustomMemoryManager::getInstance().CustomDelete(PTR, SIZE);
#define MM_DELETE_A(PTR, LENGHT, SIZE)CustomMemoryManager::getInstance().CustomDeleteA(PTR, LENGHT ,  SIZE);

#define MM_MALLOC(SIZE) CustomMemoryManager::getInstance().CustomMalloc(SIZE);
#define MM_FREE(SIZE) CustomMemoryManager::getInstance().CustomDelete(PTR, SIZE);

class CustomMemoryManager
{

public:
	CustomMemoryManager() = default;

	static CustomMemoryManager& getInstance() {
		static CustomMemoryManager instance;
		return instance;
	}

	void* CustomNew(size_t size);
	void* CustomNewA(size_t objectSize, size_t arraySize);

	void CustomDelete(void* ptr, size_t size);
	void CustomDeleteA(void* ptr, size_t objectSize, size_t arraySize);

	void CustomFree(void* ptr, size_t objectSize);
	void* CustomMalloc(size_t objectSize);

	CustomMemoryManager(const CustomMemoryManager&) = delete;
	CustomMemoryManager& operator= (const CustomMemoryManager&) = delete;
};
