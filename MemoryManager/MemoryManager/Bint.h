#pragma once
#include <deque>
#include <iosfwd>
#include <string>
#include <iostream>

//PROBLEM TO SOLVE:
//all built-in data type are limited,
//for instance, in C++, number bigger than the maximum integer
//of type _int64 must be turned to float type which is imprecise.
//It is necessary to develop a special data type to accurately
//express numbers larger than maximum integer or with much
//more decimal digits.

//Sources:
//"Implementation of Unlimited Integer" paper.
//https://github.com/sercantutar/infint/blob/master/InfInt.h.
//https://github.com/panks/BigInteger //very easy, but strings as main structure so slow lookup and setup on long values.
//https://gist.github.com/shwangdev/1330739 //vector of char, not too clear.
//https://github.com/kasparsklavins/bigint/blob/master/src/bigint.h ////vector of int, easy to use, bad code for simple methods like subtraction.
 
class Bint
{
private:
	//as of "Implementation of Unlimited Integer" paper with a 32 bit unsigned integer
	std::deque<uint32_t> m_Value; //leftmost digit position
	bool isNegative; //if true the Bint sign is negative

	void Sum(const Bint& other);
	void Sub(const Bint& other);
	Bint Mul(const Bint& other);
	Bint Div(const Bint& other);

	int CheckEquality(const Bint& other)const;

	static std::string IntToString(uint64_t num);

	struct OpResult
	{
		uint64_t  result;
		uint32_t  GetRightmostVal() const;
		uint32_t  GetLeftmostVal() const;
	};

public:

	Bint();
	Bint(int32_t val);
	Bint(const std::string& strVal);
	Bint(const Bint& other);
	~Bint();

	//Arithmetic operators
	Bint& operator=(const Bint& other);
	Bint& operator+=(const Bint& other);
	Bint& operator-=(const Bint& other);
	Bint& operator*=(const Bint& other);
	Bint& operator/=(const Bint& other);
	Bint& operator%=(const Bint& other);
	friend Bint operator+(const Bint& a);
	friend Bint operator-(const Bint& a);
	friend Bint operator+(const Bint& a, const Bint& b);
	friend Bint operator-(const Bint& a, const Bint& b);
	friend Bint operator*(const Bint& a, const Bint& b);
	friend Bint operator/(const Bint& a, const Bint& b);
	friend Bint operator%(const Bint& a, const Bint& b);
	Bint& operator++();
	Bint operator++(int);
	Bint& operator--();
	Bint operator--(int);
	
	//Relational operators
	friend bool operator<(const  Bint& a, const Bint& b);
	friend bool operator<=(const Bint& a, const Bint& b);
	friend bool operator>(const  Bint& a, const Bint& b);
	friend bool operator>=(const Bint& a, const Bint& b);
	friend bool operator==(const Bint& a, const Bint& b);
	friend bool operator!=(const Bint& a, const Bint& b);

	friend std::ostream& operator<<(std::ostream&, const Bint&);
	Bint pow(const Bint&);

	//TESTING
	//BitWise operators
	//friend Bint operator~(const Bint& a);
	//friend Bint operator&(const Bint& a, const Bint& b);
	//friend Bint operator|(const Bint& a, const Bint& b);
	//friend Bint operator^(const Bint& a, const Bint& b);
	//friend Bint operator<<(const Bint& a, const Bint& b);
	//friend Bint operator>>(const Bint& a, const Bint& b);
	Bint& operator&=(const Bint& other);
	Bint& operator|=(const Bint& other);
	Bint& operator^=(const Bint& other);
	Bint& operator<<=(const Bint& other);
	Bint& operator>>=(const Bint& other);

	void ShiftLeft(uint32_t);
	void ShiftRight(uint32_t);

	explicit operator uint32_t() const;
};