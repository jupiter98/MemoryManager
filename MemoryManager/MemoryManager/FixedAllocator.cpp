#include "FixedAllocator.h"

FixedAllocator::FixedAllocator(std::size_t blockSize)
	: m_blockSize(blockSize)
	, m_allocChunk(nullptr)
	, m_deallocChunk(nullptr)
{
	//blocks must be over size 0.
	assert(m_blockSize > 0);

	m_prev = m_next = this;

	std::size_t numBlocks = DEFAULT_CHUNK_SIZE / blockSize;
	if (numBlocks > UCHAR_MAX) numBlocks = UCHAR_MAX; //if over the value, set it to UCHAR_MAX
	else if (numBlocks == 0) numBlocks = CHAR_BIT /*8*/ * blockSize;

	m_numBlocks = static_cast<unsigned char>(numBlocks);
	assert(m_numBlocks == numBlocks);
}

FixedAllocator::FixedAllocator(const FixedAllocator& other)
	: m_blockSize(other.m_blockSize)
	, m_numBlocks(other.m_numBlocks)
	, m_chunks(other.m_chunks)
{
	m_prev = &other;
	m_next = other.m_next;
	//insert this allocator after the other one
	other.m_next->m_prev = this;
	other.m_next = this;

	if (other.m_allocChunk) //m_allocChunk is obtained by offsetting the one from other.
	{
		m_allocChunk = &m_chunks.front() + (other.m_allocChunk - &other.m_chunks.front());
	}
	else
	{
		m_allocChunk = 0;
	}
	if (other.m_deallocChunk) //same here but with m_deallocChunk
	{
		m_deallocChunk = &m_chunks.front() + (other.m_deallocChunk - &other.m_chunks.front());
	}
	else
	{
		m_deallocChunk = 0;
	}

}


FixedAllocator::~FixedAllocator()
{
	if (m_prev != this || m_next != this)
	{
		m_prev->m_next = m_next;
		m_next->m_prev = m_prev;
		m_prev = m_next = nullptr;
		return;
	}
	//check detachment from list
	assert(m_prev == m_next);

	//release all current chunks
	Chunks::iterator i = m_chunks.begin();
	for (; i != m_chunks.end(); ++i)
	{
		assert(i->m_blocksAvailable == m_numBlocks);
		i->Release();
	}
}

void* FixedAllocator::Allocate()
{
	if (m_allocChunk == 0 || m_allocChunk->m_blocksAvailable == 0)
	{
		Chunks::iterator iterator = m_chunks.begin();
		for (;; ++iterator) //search for an available chunk 
		{
			if (iterator == m_chunks.end()) //Una-Tantum (hopefully)
			{
				// Initialize a new chunk if there are no chunks available and put it inside the vector for future use. 
				m_chunks.reserve(m_chunks.size() + 1);
				Chunk newChunk;
				newChunk.Init(m_blockSize, m_numBlocks);
				m_chunks.push_back(newChunk);
				m_allocChunk = &m_chunks.back();
				m_deallocChunk = &m_chunks.front(); //?
				break;
			}
			if (iterator->m_blocksAvailable > 0)
			{
				m_allocChunk = &*iterator;
				break;
			}
		}
	}
	assert(m_allocChunk != 0);
	assert(m_allocChunk->m_blocksAvailable > 0);

	return m_allocChunk->Allocate(m_blockSize);
}


void FixedAllocator::Deallocate(void* p_block)
{
	//check chunks location and state
	assert(!m_chunks.empty());
	assert(&m_chunks.front() <= m_deallocChunk);
	assert(&m_chunks.back() >= m_deallocChunk);

	m_deallocChunk = CheckVicinity(p_block);
	if (m_deallocChunk != nullptr)
	{
		DoDeallocate(p_block);
	}
}


Chunk* FixedAllocator::CheckVicinity(void* p_block)
{
	assert(!m_chunks.empty());
	assert(m_deallocChunk);

	const std::size_t chunkLength = m_numBlocks * m_blockSize;

	Chunk* lowIterator = m_deallocChunk;
	Chunk* hiIterator = m_deallocChunk + 1;
	//get the upper and lower bounds of the list of chunks available
	Chunk* lowBound = &m_chunks.front();
	Chunk* hiBound = &m_chunks.back() + 1;

	// Special case: m_deallocChunk is the last in the vector
	if (hiIterator == hiBound)
	{
		hiIterator = 0;
	}

	for (;;) //can this also be a while loop?
	{
		if (lowIterator)
		{
			if (p_block >= lowIterator->m_pData && p_block < lowIterator->m_pData + chunkLength) //costly operation but it assures that we are in the correct position
			{
				return lowIterator;
			}
			if (lowIterator == lowBound) lowIterator = 0; //arrived at start of chunk
			else --lowIterator; //decrement until we find a correct block or we reach the low bound
		}

		if (hiIterator)
		{
			if (p_block >= hiIterator->m_pData && p_block < hiIterator->m_pData + chunkLength)
			{
				return hiIterator;
			}
			if (++hiIterator == hiBound) hiIterator = 0; //arrived at end of chunk
		}
	}
	assert(false); //always error
	return 0;
}

void FixedAllocator::DoDeallocate(void* p_block)
{
	//position check for the p_block 
	assert(m_deallocChunk->m_pData <= p_block);
	assert(m_deallocChunk->m_pData + m_numBlocks * m_blockSize > p_block);

	// call into the chunk, will adjust the inner list but won't release memory
	m_deallocChunk->Deallocate(p_block, m_blockSize);

	if (m_deallocChunk->m_blocksAvailable == m_numBlocks)
	{
		// m_deallocChunk is completely free, should we release it? Yes but we need two empty ones.

		Chunk& lastChunk = m_chunks.back();
		//if the last chunk is the one from which we just removed a block
		if (&lastChunk == m_deallocChunk)
		{
			// check if we have two last chunks empty
			if (m_chunks.size() > 1 && m_deallocChunk[-1].m_blocksAvailable == m_numBlocks)
			{
				// Two free chunks, discard the last one
				lastChunk.Release();
				m_chunks.pop_back();
				//reset pointers to front 
				m_allocChunk = m_deallocChunk = &m_chunks.front();
			}
			return;
		}

		//else check if last chunk is empty
		if (lastChunk.m_blocksAvailable == m_numBlocks)
		{
			// Two free blocks, discard one [noncontinuous]
			lastChunk.Release();
			m_chunks.pop_back();
			m_allocChunk = m_deallocChunk;
		}
		else
		{
			// move the empty chunk to the end, there is not a couple of empty chunks
			std::swap(*m_deallocChunk, lastChunk);
			m_allocChunk = &m_chunks.back();
		}
	}
}